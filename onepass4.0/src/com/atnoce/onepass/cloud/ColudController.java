/*
 * Copyright 2002-2016 the atnoce.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.cloud;

import com.atnoce.onepass.core.SecurityController;
import com.atnoce.onepass.net.BasicNetService;
import com.atnoce.onepass.utils.Cache;
import com.atnoce.onepass.utils.Const;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author atnoce.com
 * @date 2016/11/6
 * @since 1.0.0
 */
public class ColudController {

    public void checkColud(String coludAccount){

    }
    /**
     * 同步数据文件
     * @return
     */
    public Map<String,Object> sync(){
        /*
        有以下几种情况：
        服务端没有数据文件，直接上传
        服务端有数据文件，与本地文件对比校验后决定同步哪一个
        可能存在问题：多台PC使用时，其中一台使用了云账号，其他没有使用云账号，当其他PC产生数据后又配置了云账号，此时，数据文件就会有两个版本，如何合并两个数据文件？
         */
        //先不考虑，先解决正常使用问题
        //将本地文件信息发送到服务器
        //File file=new File(Cache.getDbPaht().replace("\\", "/")+"/onepass.db");
        /*File file=new File(CommonUtil.DEFAULT_DB_PATH);
        String md5=null;
        try {
        md5=FileMd5.fileMD5(file);
        } catch (IOException ex) {
        } catch (NoSuchAlgorithmException ex) {
        }*/
        Map<String, Object> exeSynch=new HashMap<>();
        try {
            exeSynch = new BasicNetService().exeSynch(SecurityController.getSecurityInstence().decryptColudAccount(Cache.getProConfig(Const.COLUDACCOUNT), Cache.getKey()));
        } catch (IOException | NoSuchAlgorithmException ex) {
            exeSynch.put("flag", false);
            exeSynch.put("msg", ex.getMessage());
        } catch (Exception ex) {
            exeSynch.put("flag", false);
            exeSynch.put("msg", ex.getMessage());
        }

        return exeSynch;
    }
}
