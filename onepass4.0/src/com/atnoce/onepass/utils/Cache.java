/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.utils;

import com.atnoce.onepass.pojo.OnepassConfig;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

/**
 * @author atnoce.com
 * @date 2016/11/4 0:02
 * @since 1.0.0
 */
public class Cache {
    private static Map<String,String> proConfig=new HashMap<>();
    private static String key="";
    private Cache(){}

    private static OnepassConfig oc;
    /**
     * 新增密码面板中被选中的分类
     */
    private static String ADD_PASS_ITEM_SELECT_DIRTYPE=null;
    /**
     * 主面板左侧分类列表中被选中的分类
     */
    private static String INDEX_LEFT_SELECTED_DIRTYPE=null;
    /**
     * 需要更新时间的数据队列
     */
    private static Queue<Map<QueueType,String>> updateModifyTimeQueue=new LinkedList<>();
    /**
     * 更新时间戳服务运行状态
     */
    public static boolean updateTimeServiceRun=false;
    public static String befortTempColudAccount="";

    /**
     * 返回要更新的数据
     * 如果没有值则会返回null
     * @return
     */
    public synchronized static Map<QueueType,String> getUpdateModifyTimeQueue(){
        if (!updateModifyTimeQueue.isEmpty()){
            return updateModifyTimeQueue.poll();
        }else{
            return null;
        }
    }

    /**
     * 向队列中添加任务项
     * @param queueItem
     */
    public static void addUpdateModifyTimeQueue(Map<QueueType,String> queueItem){

        updateModifyTimeQueue.offer(queueItem);
    }
    /**
     * 获取系统的KEY
     * @return
     */
    public static String getKey(){
        return key;
    }

    /**
     * 设置系统的KEY
     * @param key
     */
    public static void setKey(String key) {
        Cache.key = key;
    }

    /**
     * 设置系统变量，此变量设计为仅允许整体设置OnepassConfig对象，而不允许单独修改OnepassConfig对象的属性<br>
     *     如果需要更新OnepassConfig对象的一个属性，正确的方法应该是先将该属性保存至数据库，然后从数据库重新读取该对象
     * @param oc
     */
    public static void setOc(OnepassConfig oc) throws Exception {
        if (oc==null){
            throw new Exception("未获取到配置信息！");
        }
        Cache.oc = oc;
    }

    /**
     * 获取OnepassConfig对象
     * @return
     */
    public static OnepassConfig getOc() {
        return oc;
    }

    /**
     * 此变量记录了在新增密码时选择的密码分类
     * @return
     */
    public static String getAddPassItemSelectDirtype() {
        return ADD_PASS_ITEM_SELECT_DIRTYPE;
    }

    /**
     * 此变量记录了在左侧选中的分类，弹出新增密码面板时读取该变量用以初始化新增面板中默认选中的分类
     * @return
     */
    public static String getIndexLeftSelectedDirtype() {
        return INDEX_LEFT_SELECTED_DIRTYPE;
    }

    public static void setAddPassItemSelectDirtype(String addPassItemSelectDirtype) {
        ADD_PASS_ITEM_SELECT_DIRTYPE = addPassItemSelectDirtype;
    }

    public static void setIndexLeftSelectedDirtype(String indexLeftSelectedDirtype) {
        INDEX_LEFT_SELECTED_DIRTYPE = indexLeftSelectedDirtype;
    }

    /**
     * 设置properties
     * @param key
     * @param value
     */
    public static void setProConfig(String key,String value) {
        Cache.proConfig.put(key, value);
    }

    /**
     * 获取properties
     * @param key
     * @return
     */
    public static String getProConfig(Const key){
        String result="";
        if(proConfig.containsKey(key.name())){
            result=proConfig.get(key.name());
        }
        return result;
    }

    /**
     * 获取properties，如果没有值的时候返回默认值
     * @param key
     * @param defaultValue
     * @return
     */
    public static String getProConfig(Const key,String defaultValue){
        //值存在，并且不为空的时候返回值，否则返回默认值
        if (proConfig.containsKey(key.name())){
            if (StringUtils.isNotEmpty(proConfig.get(key.name()))){
                defaultValue=proConfig.get(key.name());
            }
        }
        return defaultValue;
    }
}
