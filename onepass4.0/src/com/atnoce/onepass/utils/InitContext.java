/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.utils;

import java.io.*;
import java.util.Enumeration;
import java.util.Properties;

/**
 * @author atnoce.com
 * @date 2016/11/4 0:41
 * @since 1.0.0
 */
public class InitContext {
    private InitContext(){}
    /**
     * 读取配置文件内容到内存中
     * @throws IOException
     */
    public static void init() throws IOException {
        Properties pro=new Properties();
        pro.load(new FileReader("./config/onepass.properties"));

        Enumeration<?> propertyNames = pro.propertyNames();
        while(propertyNames.hasMoreElements()){
            Object nextElement = propertyNames.nextElement();
            String get = (String)pro.get(nextElement);
            Cache.setProConfig((String)nextElement, get);
        }
    }
    /**
     * 更新配置文件并重新读取配置文件到缓存中
     * @param key
     * @param value
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void updateProperties(String key,String value) throws FileNotFoundException, IOException{
        Properties pro=new Properties();
        pro.load(new FileReader("./config/onepass.properties"));
        OutputStream fos=new FileOutputStream("./config/onepass.properties");

        pro.setProperty(key, value);
        pro.store(fos, "");
        init();//自动刷新
    }
}
