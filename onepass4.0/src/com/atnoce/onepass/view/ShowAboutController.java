/*
 * Copyright 2002-2016 the atnoce.com.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.view;

import com.atnoce.onepass.Start;
import com.atnoce.onepass.utils.CommonUtil;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author atnoce.com
 * @date 2016/11/6
 * @since 1.0.0
 */
public class ShowAboutController implements Initializable {
    private Start start;
    @FXML
    private Text aboutTop;
    @FXML
    private Text aboutcent1;

    public void setStart(Start start){
        this.start=start;
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
//        aboutTop.setText("onepass.vip");
//        aboutTop.setFont(Font.font(Font.getDefault().getFamily(),FontWeight.BOLD,36));
//        DropShadow dropShadow=new DropShadow();
//        aboutTop.setEffect(dropShadow);

        aboutcent1.setText("版本: "+ CommonUtil.APP_VERSION+"\n官网:http://www.onepass.vip");
        aboutcent1.setFont(Font.font(Font.getDefault().getFamily(), FontWeight.BOLD,14));

    }
}
