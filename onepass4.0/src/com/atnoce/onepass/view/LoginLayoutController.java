/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.atnoce.onepass.view;

import com.atnoce.onepass.Start;
import com.atnoce.onepass.core.SecurityController;
import com.atnoce.onepass.dao.OnepassConfigDao;
import com.atnoce.onepass.utils.Cache;
import com.atnoce.onepass.utils.CommonUtil;
import com.atnoce.onepass.utils.InitContext;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import org.apache.commons.lang.StringUtils;

import java.sql.SQLException;
import java.util.Optional;

/**
 * @author atnoce.com
 * @date 2016/11/4 0:50
 * @since 1.0.0
 */
public class LoginLayoutController {
    @FXML
    private PasswordField mainPass;
    @FXML
    private Button loginBtn;
    @FXML
    private ImageView loginimage;
    @FXML
    private Hyperlink exitLink;
    private Start start;
    public void setStart(Start start){
        this.start=start;
    }
    private final OnepassConfigDao cd=new OnepassConfigDao();
    @FXML
    private void initialize(){
       loginBtn.setOnAction(event -> login());
        exitLink.setOnAction(event -> exitSystem());

    }
    private void exitSystem(){
        Platform.exit();
    }

    private void login() {
        try {
            Cache.setOc(cd.getOnepassConfig());
        } catch (SQLException e) {
            CommonUtil.alert(Alert.AlertType.ERROR,"读取配置信息失败","读取配置信息时发生错误!"+e.getMessage(),"错误").showAndWait();
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            CommonUtil.alert(Alert.AlertType.ERROR,"读取配置信息失败","读取配置信息时发生错误!"+e.getMessage(),"错误").showAndWait();
            e.printStackTrace();
        }catch (Exception e){
            CommonUtil.alert(Alert.AlertType.ERROR,"读取配置信息失败","读取配置信息时发生错误!"+e.getMessage(),"错误").showAndWait();
            return;
        }
        String s=mainPass.getText();
        if (StringUtils.isEmpty(s)){
            CommonUtil.alert(Alert.AlertType.ERROR,"登录失败！","主密码不能为空!","错误").showAndWait();
            return;
        }else if (mainPass.getLength()>100){
            CommonUtil.alert(Alert.AlertType.ERROR,"登录失败！","不支持超过100字符以上的主密码!","错误").showAndWait();
            return;
        }
        if (SecurityController.getSecurityInstence().checkMainPass(s)){

            if (StringUtils.isNotEmpty(Cache.befortTempColudAccount)){
                try {
                    InitContext.updateProperties("COLUDACCOUNT", SecurityController.getSecurityInstence().encryptColudAccount(Cache.befortTempColudAccount, Cache.getKey()));
                    start.initIndexLayout();
                } catch (Exception e) {
                    Alert alert=new Alert(Alert.AlertType.CONFIRMATION);
                    alert.setContentText("错误详情："+e.getMessage());
                    alert.setTitle("错误");
                    alert.setHeaderText("写入云账号时出现问题，请点击确定尝试重新写入！");
                    Optional<ButtonType> buttonType = alert.showAndWait();
                    String typeCode1 = buttonType.get().getButtonData().getTypeCode();
                    if (StringUtils.equals("0",typeCode1)){
                        login();
                    }else{
                        start.initIndexLayout();
                    }
                }
            }else{
                start.initIndexLayout();
            }
        }else{
            CommonUtil.alert(Alert.AlertType.ERROR,null,"密码错误！","错误").showAndWait();
        }
    }
}
